#!/bin/bash

lein clean;
lein codox;
rsync -av resources/docs  schizo:public_html/cs240/;
rsync -av resources/migrations/20190520182245-init-tables.up.sql schizo:public_html/cs240/db.txt;
