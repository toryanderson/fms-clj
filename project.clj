(defproject family-map "0.1.0-SNAPSHOT"

  :description "Family Map Server & Application for CS 240"
  :url "http://example.com/FIXME"

  :dependencies [[byu-odh/byu-cas "0.1.5"]
                 [camel-snake-kebab "0.4.0"]
                 [cheshire "5.8.1"]
                 [cprop "0.1.13"]
                 [hikari-cp "2.7.0"]
                 [honeysql "0.9.4"]
                 [luminus-immutant "0.2.5"]
                 [luminus-migrations "0.6.4"]
                 [luminus-nrepl "0.1.6"]
                 [metosin/compojure-api "1.1.12"]
                 [metosin/ring-http-response "0.9.1"]
                 [mount "0.1.16"]
                 [org.clojure/clojure "1.10.0"]
                 [org.clojure/tools.cli "0.4.1"]
                 [com.taoensso/timbre "4.10.0"]
                 [org.clojure/math.numeric-tower "0.0.4"]
                 [org.xerial/sqlite-jdbc "3.27.2.1"]
                 [metosin/reitit "0.2.13"]
                 ;[metosin/reitit-schema "0.2.13"]
                 [ring-middleware-format "0.7.3"]
                 [ring/ring-defaults "0.3.2"]]
  :min-lein-version "2.0.0"
  :test-paths ["test/clj/unit" "test/clj/usecase"]
  :codox {:output-path "resources/docs"
          :namespaces [#"family-map\.db\..*"
                       #"family-map\.routes\..*"
                       #"family-map\.handler*"]
          :metadata {:doc/format :markdown}}
  :jvm-opts ["-server" "-Dconf=.lein-env"]
  :source-paths ["src/clj" "src/cljc"]
  :resource-paths ["resources"]
  :target-path "target/"
  :main family-map.core
  :plugins [[lein-cprop "1.0.1"]
            [migratus-lein "0.4.3"]
            [lein-immutant "2.1.0"]]
  :clean-targets ^{:protect false} [:target-path]
  :immutant {:war {:name "family-map%t"}}
  
  :profiles
  {:uberjar {:omit-source true
             :prep-tasks ["compile"]
             :aot :all
             :uberjar-name "family-map.jar"
             :source-paths ["env/prod/clj"]
             :resource-paths ["env/prod/resources"]}

   :dev           [:project/dev :profiles/dev]
   :test          [:project/dev :project/test :profiles/test]


   :project/dev  {:dependencies [[binaryage/devtools "0.9.10"]
                                 [pjstadig/humane-test-output "0.9.0"]
                                 [prone "1.6.1"]
                                 [ring/ring-devel "1.7.1"]
                                 [ring/ring-mock "0.3.2"]]
                  :plugins      [[com.jakemccrary/lein-test-refresh "0.14.0"]
                                 [lein-doo "0.1.7"]
                                 [lein-figwheel "0.5.12"]]
                  :source-paths ["env/dev/clj" "test/clj/unit" "test/clj/usecase"]
                  :resource-paths ["env/dev/resources"]
                  :injections [(require 'pjstadig.humane-test-output)
                               (pjstadig.humane-test-output/activate!)]}
   :project/test {:resource-paths ["env/test/resources"]}
   :profiles/dev {:repl-options {:init-ns user}}
   :profiles/test {}})
