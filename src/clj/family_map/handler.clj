(ns family-map.handler
  "This handler sets up the routes specified elsewhere and links them to defined middleware, which in turn serve to format/extend response-types. Essentially, these are the `service classes`."
  (:require [family-map.layout :refer [error-page]]
            [family-map.routes.home :refer [home-routes]]
            [family-map.routes.api :refer [api-routes]]
            [reitit.ring :as ring]
            [family-map.env :refer [defaults]]
            [mount.core :as mount]))

(mount/defstate init-app
  :start ((or (:init defaults) identity))
  :stop  ((or (:stop defaults) identity)))

(mount/defstate app
  :start
  (ring/ring-handler
   (ring/router
    [(home-routes)
     (api-routes)])
   (ring/routes
    (ring/create-resource-handler
     {:path "/"})
    (ring/create-default-handler
     {:not-found
      (constantly (error-page {:status 404, :title "404 - Page not found"}))
      :method-not-allowed
      (constantly (error-page {:status 405, :title "405 - Not allowed"}))
      :not-acceptable
      (constantly (error-page {:status 406, :title "406 - Not acceptable"}))}))))
