(ns family-map.data.people
  "NS for facilitating event manipulation"
  (:require [family-map.db.people :as people]
            [taoensso.timbre :as log]))

(def ^:private format-from-JSON-map
  {:firstName :first-name
   :lastName :last-name
   :gender :gender
   :personID :id
   :fatherID    :father-id
   :motherID   :mother-id
   :spouseID   :spouse-id
   :descendant :associated-username
   :associatedUsername :associated-username})

(def ^:private format-to-JSON-map
  {:first-name "firstName"
   :last-name "lastName"
   :gender "gender"
   :id "personID"
   :father-id "fatherID"
   :mother-id "motherID"
   :spouse-id "spouseID"
   :associated-username "associatedUsername"})


(defn map-from-json
  "Convert a keywordized json map `jm` to a compatible event map"
  [jm]
  (into {}
        (for [[k v] jm]
          [(format-from-JSON-map k) v])))

(defn map-to-json
  "Convert a map as we get from the DB into a json map"
  [em]
  (into {}
        (for [[k v] em]
          [(format-to-JSON-map k) v])))

(defn load-from-json
  "Load people from json. No validation is performed, so make sure your associated-username is correct."
  [json-maps]
  (doseq [m json-maps]
    (people/CREATE (map-from-json m))))
