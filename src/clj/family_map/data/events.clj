(ns family-map.data.events
  "NS for facilitating event manipulation"
  (:require [family-map.db.events :as events]
            [taoensso.timbre :as log]))

(def ^:private format-from-JSON-map
  {:eventType :event-type
   :personID :person-id
   :descendant :associated-username
   :associatedUsername :associated-username
   :city :city
   :longitude :longitude 
   :year :event-year
   :latitude :latitude
   :country :country 
   :eventID :id})

(def ^:private format-to-JSON-map
  {:event-type "eventType"
   :person-id "personID"
   :associated-username "associatedUsername"
   :city "city"
   :longitude "longitude"
   :event-year "year"
   :latitude "latitude"
   :country "country"
   :id "eventID"})

(defn map-from-json
  "Convert a keywordized json map `jm` to a compatible event map"
  [jm]
  (into {}
        (for [[k v] jm]
          [(format-from-JSON-map k) v])))

(defn map-to-json
  "Convert a map as we get from the DB into a json map"
  [em]
  (log/info (str "map-to-json with \n" (prn-str em)))
  (into {}
        (for [[k v] em]
          [(format-to-JSON-map k) v])))

(defn load-from-json
  "Load events from json. No validation is performed."
  [json-maps]
  (doseq [m json-maps]
    (events/CREATE (map-from-json m))))
