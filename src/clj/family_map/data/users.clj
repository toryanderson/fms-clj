(ns family-map.data.users
  "NS for facilitating event manipulation"
  (:require [family-map.db.users :as users]
            [taoensso.timbre :as log]
            [cheshire.core :as json]))

(def ^:private format-from-JSON-map
  {:userName :id
   :password :password
   :email :email
   :firstName :first-name
   :lastName :last-name
   :gender :gender
   :personID :person-id})

(def ^:private format-to-JSON-map
  {:id         "userName"
   :password   "password"
   :email         "email"
   :first-name "firstName"
   :last-name  "lastName"
   :gender       "gender"
   :person-id  "personID"})

(defn map-from-json
  "Convert a keywordized json map `jm` to a compatible event map"
  [jm]
  (into {}
        (for [[k v] jm]
          [(format-from-JSON-map k) v])))

(defn map-to-json
  "Convert a map as we get from the DB into a json map"
  [em]
  (into {}
        (for [[k v] em]
          [(format-to-JSON-map k) v])))

(defn load-from-json
  "Load users from json. No validation is performed, so mind you handle person-id properly."
  [json-maps]
  (doseq [m json-maps]
    (users/CREATE (map-from-json m))))
