(ns family-map.layout
  (:require
   [hiccup.core :as hc]
   [hiccup.page :as hp]
   [hiccup.util :refer [with-base-url]]
   [hiccup.element :refer [javascript-tag]]
   [cheshire.core :refer [generate-string]]
   [ring.util.http-response :refer [content-type ok]]
   [ring.util.anti-forgery :refer [anti-forgery-field]]
   [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]))

(defn error-page
  "error-details should be a map containing the following keys:
   :status - error status
   :title - error title (optional)
   :message - detailed error message (optional)
   returns a response map with the error page as the body
   and the status specified by the status key"
  [error-details]
  {:status  (:status error-details)
   :headers {"Content-Type" "text/html; charset=utf-8"}
   :body (hp/html5
          [:div.alert.alert-warning
           [:h1 (or (:title error-details) (str "Error " (:status error-details)))]
           [:div.error-details (:message error-details)]])})

