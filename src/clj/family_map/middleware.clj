(ns family-map.middleware
  (:require [family-map.env :refer [defaults]]
            [family-map.layout :refer [error-page]]
            [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
            [ring.middleware.format :refer [wrap-restful-format]]
            [family-map.config :refer [env]]
            [family-map.db.authorization-tokens :refer [user-for-token]]
            [ring.middleware.flash :refer [wrap-flash]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [taoensso.timbre :as log]
            [ring.util.http-response :as http])
  (:import [javax.servlet ServletContext]))

(defn wrap-internal-error [handler]
  (fn [req]
    (try
      (handler req)
      (catch Throwable t
        (log/error t)
        (error-page {:status 500
                     :title "We encountered an error with your request"
                     :message "Unhelpful Error."})))))

(defn print-handler [handler]
  (fn [req]
    (println "\n\n-----Handler is:")
    (println (str handler))
    (println "\n\n-----Req is:")
    (println (str req))
    (handler req)))

(defn wrap-formats [handler]
  (let [wrapped (wrap-restful-format
                  handler
                  {:formats [:json-kw :transit-json :transit-msgpack]})]
    (fn [request]
      ;; disable wrap-formats for websockets
      ;; since they're not compatible with this middleware
      ((if (:websocket? request) handler wrapped) request))))

(defn wrap-fms-auth
  "Check the authorization header for a token, and associate the username with it"
  [handler]
  (fn [req]
    (let [token (get-in req [:headers "authorization"])
          user-id (user-for-token token)]
      (if user-id
        (handler (assoc req :user-id user-id))
        (do
          (log/error "fms-auth failed!")
          (handler (assoc req :error
                          (ex-info "Invalid auth token" {:cause :invalid-token
                                                         :token token}))))))))

(defn wrap-base [handler]
  (-> ((:middleware defaults) handler)
      wrap-flash
      (wrap-defaults
        (-> site-defaults
            (assoc-in [:security :anti-forgery] false)
            (dissoc :session)))
      wrap-internal-error))
