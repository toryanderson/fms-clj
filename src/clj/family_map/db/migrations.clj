(ns family-map.db.migrations
  "This ns manages database migrations, including initialization of blank tables."
  (:require
   [family-map.db.core :refer [*db* dbdo!]]
   [migratus.core :as migratus]
   [family-map.config :refer [env]]))

(defn ^:private adapt-hikari-to-migratus [config]
  {:store :database
   :migration-dir "migrations/"
   :init-script "init.sql"
   :migration-table-name "migrations"
   :db config})

(defn ^:private config [] (adapt-hikari-to-migratus (-> env :family-map :db)))

(defn ^:private init []
  (migratus/init (config)))

(defn ^:private reset [] (migratus/reset (config)))

(defn renew
  "Initialize and reset the database to its base, with all tables empty"
  []
  (init)
  (reset))
;(renew)
