(ns family-map.db.authorization-tokens
  "Functions mediating transactions upon the `tokens` database table.   This is essentially the `DAO` for auth tokens."
  (:require [family-map.db.core :as db]
            [honeysql.core :as sql]
            [taoensso.timbre :as log])
  (:import [java.util
            UUID]))

(def CREATE
  "Function to create a new Authorization Token; partial of `family-map.db.core/CREATE`"
  (partial db/CREATE :authorization-tokens))
(def READ
  "Function to read a Authorization Token; partial of `family-map.db.core/READ`"
  (partial db/READ :authorization-tokens))
(def UPDATE
  "Function to update a Authorization Token with given `id`; partial of `family-map.db.core/UPDATE`"
  (partial db/UPDATE :authorization-tokens))
(def DELETE
  "Function to delete a Authorization Token with `id`; partial of `family-map.db.core/DELETE`"
  (partial db/DELETE :authorization-tokens))

(defn user-for-token
  "get the user for a given `token-id`"
  [token-id]
  (READ token-id [:username]))

(defn generate-token
  "Generate a new authorization-token for `user-id`, returning it"
  [user-id]
  (let [id (str (UUID/randomUUID))]
    (log/info (str "Creating token with id " id))
    (CREATE {:username user-id :id id})
    (READ id)))

(defn tokens-for-user
  "Get all tokens for `user-id`"
  [user-id]
  (-> {:select [:*]
       :from [:authorization-tokens]
       :where [:= :username user-id]}
      sql/format
      db/dbr))

(defn delete-for-user
  "Delete all with username `user-id`"
  [user-id]
  (-> {:delete []
       :from [:authorization-tokens]
       :where [:= :username user-id]}
      sql/format
      db/dbdo!))
