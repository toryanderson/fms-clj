(ns family-map.db.core
  "The base class for all db CRUD operations. Clojure models object-less data as mostly vectors and maps, and db results become vectors of maps."
  (:require
   [clojure.java.jdbc :as jdbc]
   [hikari-cp.core :as hik]
   [family-map.config :refer [env]]
   [mount.core :refer [defstate]]
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :refer [transform-keys]]
   [taoensso.timbre :as log]
   [honeysql.core :as sql])
  (:import java.sql.Array
           clojure.lang.IPersistentMap
           clojure.lang.IPersistentVector
           [java.sql
            BatchUpdateException
            Date
            Timestamp
            PreparedStatement]))
(defn run-init-db
  "Init the DB, turning on foreign keys"
  []
  (let [ds (hik/make-datasource (-> env :family-map :db))
        ;pragma "PRAGMA foreign_keys = ON;"
        ;init-pragma (jdbc/execute! {:datasource ds} pragma)
        ]
    #_(log/info (str "Init pragma:\n" (prn-str init-pragma)))
    ds))


(defstate ^:dynamic *db*
  :start {:datasource (run-init-db)}
  :stop (hik/close-datasource (:datasource *db*)))

(defn ^:private to-date [^java.sql.Date sql-date]
  (-> sql-date (.getTime) (java.util.Date.)))

(extend-protocol jdbc/IResultSetReadColumn
  Date
  (result-set-read-column [v - -] (to-date v))

  Timestamp
  (result-set-read-column [v - -] (to-date v))

  Array
  (result-set-read-column [v - -] (vec (.getArray v))))

(extend-type java.util.Date
  jdbc/ISQLParameter
  (set-parameter [v ^PreparedStatement stmt ^long idx]
    (.setTimestamp stmt idx (Timestamp. (.getTime v)))))

(extend-type clojure.lang.IPersistentVector
  jdbc/ISQLParameter
  (set-parameter [v ^java.sql.PreparedStatement stmt ^long idx]
    (let [conn      (.getConnection stmt)
          meta      (.getParameterMetaData stmt)
          type-name (.getParameterTypeName meta idx)]
      (when-let [elem-type (when (= (first type-name) \_) (apply str (rest type-name)))]
        (.setObject stmt idx (.createArrayOf conn elem-type (to-array v)))))))

;; CRUD
(defn dbdo! [s] (jdbc/execute! *db* s))

(defn dbc!
  "A thin wrapper for `jdbc/insert!`"
  [table-key entrymap] (jdbc/insert! *db* table-key entrymap))

(defn dbr
  "A thin wrapper for `jdbc/query`"
  [s] (map (partial transform-keys csk/->kebab-case-keyword) (jdbc/query *db* s)))

(defn dbu!
  "A thin wrapper for `jdbc/update!`"
   [table-key valmap where-vec] (jdbc/update! *db* table-key valmap where-vec))

(defn dbd!
  "A thin wrapper for `jdbc/delete!`"
  ([table-key] (let [table-name (csk/->snake_case_string table-key) ]
                 (dbdo! (str "DELETE FROM " table-name))))
  ([table-key s] (jdbc/delete! *db* table-key s)))

(defn CREATE
  "Generic item creation"
  [table-keyword valmap]
  (first (dbc! (csk/->snake_case_keyword table-keyword)
               (transform-keys csk/->snake_case_keyword valmap))))

(defn READ
  "Get anything from table by id, or all without id"
  [table-keyword &[id select-field-keys]]
  (cond-> {:select (or select-field-keys [:*])
           :from [table-keyword]}
    id (assoc :where [:= :id id])
    1 sql/format
    1 dbr
    id first
    (= 1 (count select-field-keys)) (#((first select-field-keys) %))))

(defn UPDATE
  "Update anything from table by id"
  [table-keyword id valmap]
  (let [tk (csk/->snake_case_keyword table-keyword)
        valmap (transform-keys csk/->snake_case_keyword valmap)]
    (dbu! tk valmap ["id = ?" id])
    (transform-keys csk/->kebab-case-keyword
                    (READ tk id))))

(defn DELETE
  "Generic delete"
  ([table-keyword]
   (dbd! table-keyword))
  ([table-keyword id]
   (dbd! (csk/->snake_case_keyword table-keyword) ["id = ?" id])))
