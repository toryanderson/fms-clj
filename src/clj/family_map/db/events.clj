(ns family-map.db.events
  "Functions mediating transactions upon the `events` database table.
  This is essentially the `DAO` for events."
  (:require [family-map.db.core :as db]
            [honeysql.core :as sql]))

(def BIRTH "birth")

(def CREATE
  "Function to create a new event; partial of `family-map.db.core/CREATE`"
  (partial db/CREATE :events))

(def READ
  "Function to read an event  by its `id`, or all events if no id is specified."
  (partial db/READ :events))

(def UPDATE
  "Function to update a event with given `id` according to `update-map`"
  (partial db/UPDATE :events))

(def DELETE
  "Function to delete an event with `id`"
  (partial db/DELETE :events))

(defn get-for
  "Get events for `person-id`, possibly just those of optional `event-type`"
  [person-id & [event-type]]
  (cond-> {:select [:*]
           :from [:events]
           :where [:and [:= :person-id person-id]]
           }
    event-type (update :where conj [:= :event-type event-type])
    :always sql/format
    :always db/dbr))

(defn birthyear
  "Get the year on the birth event for `person-id`"
  [person-id]
  (-> (get-for person-id "birth") ;; TODO is birth or Birth?
      first
      :event-year))

(defn delete-for-user
  "Delete all with username `user-id`"
  [user-id]
  (-> {:delete []
       :from [:events]
       :where [:= :associated-username user-id]}
      sql/format
      db/dbdo!))

(defn events-for-user
  "Get all the events whose associated-username is `username`"
  [username]
  (-> {:select [:*]
       :from [:events]
       :where [:= :associated-username username]}
      sql/format
      db/dbr))
