(ns family-map.db.users
  "Functions mediating transactions upon the `users` database table. This is essentially the `DAO` for users."
  (:require [family-map.db.core :as db]
            ;[honeysql.core :as sql]
            ))

(def CREATE
  "Function to create a new user; partial of `family-map.db.core/CREATE`"
  (partial db/CREATE :users))
(def READ
  "Function to read a user; partial of `family-map.db.core/READ`"
  (partial db/READ :users))
(def UPDATE
  "Function to update a user with given `id`; partial of `family-map.db.core/UPDATE`"
  (partial db/UPDATE :users))
(def DELETE
  "Function to delete a user with `id`; partial of `family-map.db.core/DELETE`"
  (partial db/DELETE :users))

(defn valid-password
  "Check whether `password` is valid for `user-name`"
  [user-name password]
  (when (every? some? [user-name password])
    (let [correct-pass (READ user-name [:password])]
      (= password correct-pass))))
