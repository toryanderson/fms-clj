(ns family-map.db.people
  "Functions mediating transactions upon the `people` database table. This is essentially the `DAO` for people."
  (:require [family-map.db.core :as db]
            [honeysql.core :as sql]))

(def CREATE
  "Function to create a new Authorization Token; partial of `family-map.db.core/CREATE`"
  (partial db/CREATE :people))
(def READ
  "Function to read a Authorization Token; partial of `family-map.db.core/READ`"
  (partial db/READ :people))
(def UPDATE
  "Function to update a Authorization Token with given `id`; partial of `family-map.db.core/UPDATE`"
  (partial db/UPDATE :people))
(def DELETE
  "Function to delete a Authorization Token with `id`; partial of `family-map.db.core/DELETE`"
  (partial db/DELETE :people))

(defn find-child-id
  "Find the id of the child of `parent-id`"
  [parent-id]
  (-> {:select [:id]
       :from [:people]
       :limit 1
       :where [:or
               [:= :father-id parent-id]
               [:= :mother-id parent-id]]}
      sql/format
      db/dbr
      first
      :id))

(defn delete-for-user
  "Delete all with username `user-id`"
  [user-id]
  (-> {:delete []
       :from [:people]
       :where [:= :associated-username user-id]}
      sql/format
      db/dbdo!))

(defn people-for-user
  "Get all the people whose associated-username is `username`"
  [username]
  (-> {:select [:*]
       :from [:people]
       :where [:= :associated-username username]}
      sql/format
      db/dbr))
