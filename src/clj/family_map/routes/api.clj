(ns family-map.routes.api
  "Routes providing the functionality of the program; require successful login/auth.
  This namespace is equivalent to request and result classes."
  (:require [family-map.layout :as layout]
            [family-map.middleware :as middleware]
            [ring.util.http-response :as http]
            [taoensso.timbre :as log]
            [family-map.db.people :as people]
            [family-map.db.events :as events]
            [family-map.data.people :as datp]
            [family-map.data.events :as datv]))

(defn get-all-family
  "Returns ALL family members of the current user. The current user is
  determined from the provided auth token.
  Errors: Invalid auth token, Internal server error
Success Response Body: The response body returns a JSON object with a “data” attribute that
contains an array of Person objects. Each Person object has the same format as described in
previous section on the /person/[personID] API.
{
\"data\": [ ​ /* Array of Person objects */ ​ ]
}
Error Response Body:
{
“message”: “Description of the error”
}"
  [{:as request
    user-id :user-id}]
  (cond
    (:error request)
    (do (log/warn (str "Request had an error: \n" (prn-str (:error request))))
        (http/unauthorized {"message" "Invalid token"}))
    (not user-id)
    (do (log/warn (str "Request had no user"))
        (http/unauthorized {"message" "No usr-id found"}))
    :else
    (let [people (people/people-for-user user-id)]
      (http/ok (map datp/map-to-json people)))))


(defn get-person
  "Success Response Body:
  {
  \"descendant\": \"susan\",
\"personID\": \"7255e93e\",
\"firstName\": \"Stuart\",
\"lastName\": \"Klocke\",
\"gender\": \"m\",
“father”: “7255e93e”
“mother”: “f42126c8”
\"spouse\":\"f42126c8\"
// Name of user account this person belongs to
// Person’s unique ID
// Person’s first name
// Person’s last name
// Person’s gender (“m” or “f”)
// ID of person’s father ​ [OPTIONAL, can be missing]
// ID of person’s mother ​ [OPTIONAL, can be missing]
// ID of person’s spouse ​ [OPTIONAL, can be missing]
}
Error Response Body:
{
“message”: “Description of the error”
}
  "
  [{:as request
    requester :user-id
    {person-id :person-id} :path-params}]
  #_(log/info (str "get-person received request:\n" (prn-str request)))
  (if-let [e (:error request)]
    (do (log/info (str "Request had an error: \n" (prn-str e)))
        (http/unauthorized {"message" "Invalid token"}))
    (let [person (people/READ person-id)]
      (if-not (= requester (:associated-username person))
        (http/unauthorized {"message" (str "User " requester " not authorized to view " person-id)})
        (http/ok (datp/map-to-json person))))))

(defn get-event
  "Returns the single Event object with the specified ID.

  Success Response Body:
{
\"descendant\": \"susan\"
// Name of user account this event belongs to (non-empty
// string)
\"eventID\": \"251837d7\",
// Event’s unique ID (non-empty string)
\"personID\": \"7255e93e\",
// ID of the person this event belongs to (non-empty string)
\"latitude\": 65.6833,
// Latitude of the event’s location (number)
\"longitude\": -17.9,
// Longitude of the event’s location (number)
\"country\": \"Iceland\",
// Name of country where event occurred (non-empty
// string)
\"city\": \"Akureyri\",
// Name of city where event occurred (non-empty string)
\"eventType\": \"birth\",
// Type of event (“birth”, “baptism”, etc.) (non-empty string)
\"year\": 1912,
// Year the event occurred (integer)
}
Error Response Body:
{
“message”: “Description of the error”
  }"
  [{:as request
    requester :user-id
    {event-id :event-id} :path-params}]
  #_(log/info (str "get-event received request:\n" (prn-str request)))
  (if-let [e (:error request)]
    (do (log/info (str "Request had an error: \n" (prn-str e)))
        (http/unauthorized {"message" "Invalid token"}))
    (let [event (events/READ event-id)]
      (if-not (= requester (:associated-username event))
        (http/unauthorized {"message" (str "User " requester " not authorized to view " event-id)})
        (http/ok (datv/map-to-json event))))))

(defn get-all-events
  "​ Returns ALL events for ALL family members of the current user. The current
  user is determined from the provided auth token.

  Request Body: None

  Errors: Invalid auth token, Internal server error
Success Response Body: The response body returns a JSON object with a “data” attribute that
contains an array of Event objects. Each Event object has the same format as described in
previous section on the /event/[eventID] API.
  "
  [{:as request
    user-id :user-id}]
(cond
    (:error request)
    (do (log/warn (str "Request had an error: \n" (prn-str (:error request))))
        (http/unauthorized {"message" "Invalid token"}))
    (not user-id)
    (do (log/warn (str "Request had no user"))
        (http/unauthorized {"message" "No usr-id found"}))
    :else
    (let [events (events/events-for-user user-id)]
      (http/ok (map datv/map-to-json events)))))

(defn api-routes
  "Routes requiring valid `auth-tokens`, specifically `/person` and `/event` routes"
  []
  [""
   {:middleware [middleware/wrap-base
                 middleware/wrap-formats
                 middleware/wrap-fms-auth]}
   ["/person"
    ["" {:get get-all-family}]
    ["/:person-id" {:get get-person}]]
   ["/event"
    [""  {:get get-all-events}]
    ["/:event-id" {:get get-event}]]])
