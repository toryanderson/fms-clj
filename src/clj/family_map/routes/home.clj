(ns family-map.routes.home
  "Basic route(s) requiring no authentication"
  (:require [family-map.layout :as layout]
            [ring.util.response :as response]
            [ring.util.http-response :as http]
            [family-map.middleware :as middleware]
            [family-map.db.users :as users]
            [family-map.db.people :as people]
            [family-map.db.events :as events]
            [family-map.db.authorization-tokens :as token]
            [family-map.data :as data]
            [clojure.java.io :as io]
            [taoensso.timbre :as log]))

(defn home-page
  "Default response, providing the basic html provided page"
  [_]
  (-> (http/ok (slurp (io/resource "public/index.html")))
      (http/header "Content-Type" "text/html")))

(defn register-user
  "Receive users registration information and create appropriate DB entries. Return valid authcode.

  Success Response Body:
{
\"authToken\": \"cf7a368f\",
// Non-empty auth token string
\"userName\": \"susan\",
// User name passed in with request
\"personID\": \"39f9fe46\"
// Non-empty string containing the Person ID of the
// user’s generated Person object
}
Error Response Body:
{
“message”: “Description of the error”
  }
"
  [{:as request
    {:as params
     :keys [userName password email firstName lastName gender]} :params}]
  (log/info (str "\n\nregister-user received:\n" (prn-str params)))
  (let [user-map {:email email
                  :first-name firstName
                  :gender gender
                  :id userName
                  :last-name lastName
                  :password password}]
    (try 
      (do
        (when-not (every? some? (vals user-map))
          (throw (ex-info "Missing required values in user-map" {:user-map user-map})))
        (data/initialize-and-genesis! user-map)
        (http/ok {"authToken" (:id (token/generate-token (:id user-map)))
                  "userName" userName
                  "personID" (users/READ userName [:person-id])}))
      (catch Exception e
        #_(throw (ex-info "Error trying to register" {:error e}))
        (http/bad-request {"description" (.toString e)})))))

(defn user-login
  "Log the user in and return a new auth token.

  Request Body:
{
\"userName\": \"susan\",
// Non-empty string
\"password\": \"mysecret\"
// Non-empty string
}
Errors: Request property missing or has invalid value, Internal server error
Success Response Body:
{
\"authToken\": \"cf7a368f\",
// Non-empty auth token string
\"userName\": \"susan\",
// User name passed in with request
\"personID\": \"39f9fe46\"
// Non-empty string containing the Person ID of the
// user’s generated Person object
  }

  Error Response Body:
{
“message”: “Description of the error”
}"
  [{{:as parmap
     :keys [userName password]} :params}]
  (log/info (str "In user-login, was given params:\n" (prn-str parmap) ))
  (if (users/valid-password userName password)
    (let [token (:id (token/generate-token userName))
          user (users/READ userName)]
      (http/ok {"authToken" token
                "userName" userName
                "personID" (:person-id user)}))
    (do
      (log/warn (str "Invalid login for " userName " with pass: " password))
      (http/unauthorized {"message" "Incorrect username or password provided."}))))

(defn fill-generations
  "Populates the server's database with generated data for the specified user name.
The required \"username\" parameter must be a user already registered with the server. If there is
any data in the database already associated with the given user name, it is deleted. The
optional “generations” parameter lets the caller specify the number of generations of ancestors
to be generated, and must be a non-negative integer (the default is 4, which results in 31 new
  persons each with associated events).

  Success Response Body:
{
“message”: “Successfully added X persons and Y events to the database.”
  }

  Error Response Body:
{
  “message”: “Description of the error”
  }
  "
  [{{:as path-params
     :keys [username generations]}
    :path-params}]
  (log/info (str "fill-route received \n" (prn-str path-params)))
  (if-let [user (users/READ username)]
    (let [_ (log/info (str "fill-route found user \n" (prn-str user)))
          _remove-user-stuff (data/clear-for-user (:id user))
          generations (Integer. generations)
          pre-vals {:users (count (users/READ))
                    :people (count (people/READ))
                    :events (count (events/READ))}
          _ (log/info (str "After removing user, values are: \n" (prn-str pre-vals)))
          gen-message #(format "Successfully added %d persons and %d events to the database."
                               (- (count (people/READ))
                                  (inc (:people pre-vals)))
                               (- (count (events/READ))
                                  (:events pre-vals)))]
      (if (neg? generations)
        (http/bad-request {"message" (str "Invalid generations: " generations)})
        (do         
          (data/initialize-and-genesis! (dissoc user :person-id) generations)
          (http/ok {"message" (gen-message)}))))
    (http/bad-request {"message" (str "User " username " not found")})))

(defn clear-db
  "Deletes ALL data from the database, including user accounts, auth tokens, and
  generated person and event data.

  Request Body: None
Errors: Internal server error
Success Response Body:
{
“message”: “Clear succeeded.”
}
Error Response Body:
{
“message”: “Description of the error”
}"
  [_request]
  (try
    (do 
      (data/clear-db!)
      (http/ok {"message" "Clear succeeded."}))
    (catch Exception e (http/bad-request {"message" (str e)}))))



(defn load-db
  "Clears all data from the database (just like the /clear API), and then loads the
  posted user, person, and event data into the database.

  The “users” property in the request body contains an array of users to be
created. The “persons” and “events” properties contain family history information for these
users. The objects contained in the “persons” and “events” arrays should be added to the
server’s database. The objects in the “users” array have the same format as those passed to
the /user/register API with the addition of the personID. The objects in the “persons” array have
the same format as those returned by the /person/[personID] API. The objects in the “events”
array have the same format as those returned by the /event/[eventID] API.
{
“users”: [ ​ /* Array of User objects */ ​ ],
“persons”: [ ​ /* Array of Person objects */ ​ ],
“events”: [ ​ /* Array of Event objects */ ​ ]
}
Errors: Invalid request data (missing values, invalid values, etc.), Internal server error
Success Response Body:
{
“message”: “Successfully added X users, Y persons, and Z events to the database.”
}
Error Response Body:
{
“message”: “Description of the error”
}"
  [{{:as params
     :keys [users persons events]} :params
    :as _request}]
  (log/info (str "load path received params:" (prn-str params)))
  (try
    (do 
        (data/load-data-from-json params)
        (http/ok {"message" (format "Successfully added %d users, %d persons, and %d events to the database."
                                    (count users)
                                    (count persons)
                                    (count events))}))
    (catch Exception e
      (throw (ex-info "Error during load path" {:error e}))
      #_(http/bad-request {"message" (str e)}))))

(defn home-routes
  "The basic routes to be handled which don't require any auth token: /, /user login and registration, /clear, /fill*, and /load"
  []
  [""
   {:middleware [middleware/wrap-base
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/user"
    ["/register" {:post register-user}]
    ["/login" {:post user-login}]]
   ["/clear" {:post clear-db}]
   ["/load" {:post load-db}]
   ["/fill/:username/:generations" {:post fill-generations}]])
