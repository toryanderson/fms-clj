(ns family-map.data
  "NS for initializing data"
  (:require [family-map.db.users :as users]
            [family-map.db.people :as people]
            [family-map.db.events :as events]
            [family-map.db.authorization-tokens :as tokens]
            [family-map.data.people :as datp]
            [family-map.data.events :as datv]
            [family-map.data.users :as datu]
            [clojure.java.io :as io]
            [taoensso.timbre :as log]
            [cheshire.core :as json])
  (:import [java.util
            UUID]))

(def RANDOM-EVENTS ["Military Enlistment"
                    "Wrote Last Novel"
                    "Went Blind"
                    "Won Lottery"
                    "Got First House"
                    "Christening"])

(def LOCATIONS
  (let [json-file (io/resource "json/locations.json")]
    (-> json-file slurp json/parse-string (get "data"))))

(def SURNAMES
  (let [json-file (io/resource "json/snames.json")]
    (-> json-file slurp json/parse-string (get "data"))))

(def MALE-GIVEN-NAMES
  (let [json-file (io/resource "json/mnames.json")]
    (-> json-file slurp json/parse-string (get "data"))))

(def FEMALE-GIVEN-NAMES
  (let [json-file (io/resource "json/fnames.json")]
    (-> json-file slurp json/parse-string (get "data"))))

(defn clear-db!
  "Clear everything from the db"
  []
  (do
    (users/DELETE)
    (people/DELETE)
    (events/DELETE)
    (tokens/DELETE)))

(defn clear-for-user
  "Clear out all content for a user"
  [user-id]
  (doto user-id
    (users/DELETE)
    (people/delete-for-user)
    (events/delete-for-user)
    (tokens/delete-for-user)))

(defn load-data-from-json
  "Load data received from a keywordized json submission, containing `users`, `persons`, and `vents`"
  [{:as data
    :keys [users persons events]}]
  ;;(log/info (str "load-data Received params " (prn-str data)))
  (datu/load-from-json users)
  (datp/load-from-json persons)
  (datv/load-from-json events))


(defn random-location
  "Select a random location from the json"
  []
  (rand-nth LOCATIONS))

(defn gen-male-given-name
  "Generate a male given (first) name, drawing on the json"
  []
  (rand-nth MALE-GIVEN-NAMES))

(defn gen-surname
  "Generate a given (last) name, drawing on the json"
  []
  (rand-nth SURNAMES))

(defn gen-female-given-name
  "Generate a female given (first) name, drawing on the json"
  []
  (rand-nth FEMALE-GIVEN-NAMES))

(defn derive-person-map
  "Derive a person map appropriate to `user-map`"
  [{:as _user-map
    :keys [id first-name last-name gender person-id]
    :or {person-id id}}] ;; here could be changed if we want random ids here (would require test alteration)
  {:id person-id
   :associated-username id
   :first-name first-name
   :last-name last-name
   :gender gender})

(defn event-dates
  "Generate the event-dates for core events based on `generation-n`"
  [generation-n]
  (let [this-year 2019
        birthyear (- this-year (* generation-n 20))
        marriage (+ birthyear 18)
        ;death (+ birthyear 78)
        ]
    {:birth birthyear
     :marriage marriage
     :other (- this-year (rand-int (- this-year birthyear)))}))

(defn random-id [] (str (UUID/randomUUID)))

(defn generate-birth
  "Generate the birth event in `birth-year`for `person-id`"
  [{:keys [username person-id birth-year]}]
  (let [location-map (random-location)
        event-map (merge location-map {:id (random-id)
                                       :associated-username username
                                       :person-id person-id
                                       :event-type "birth"
                                       :event-year birth-year})]
    (try
      (events/CREATE event-map)
      (catch Exception e
        (throw (ex-info "Error establishing birth" {:event-map event-map
                                                    :user (users/READ username)
                                                    :person (people/READ person-id)
                                                    :error (str e)}))))))

(defn generate-random-event
  "Generate a random event for `person-id` of `generation-n`"
  [{:keys [username person-id generation-n event-type]
    :or {event-type (rand-nth RANDOM-EVENTS)}}]
  (let [event-dates (event-dates generation-n)
        location-map (random-location)]
    (try
      (events/CREATE (merge location-map {:id (random-id)
                                          :associated-username username
                                          :person-id person-id
                                          :event-type event-type
                                          :event-year (:other event-dates)}))
      (catch Exception e
        (throw (ex-info (str "Error establishing random " event-type) {:error e}))))))

(defn calculate-parent-marriage-year
  "Given a `child-id`, calculate the marriage year of the parents based on the child's birth year"
  [child-id]
  (if-let [birthyear (events/birthyear child-id)]
    (- birthyear 2)
    (throw (ex-info (str "Birthyear not found for child " child-id) {:child-id child-id}))))

(defn initialize-person!
  "Create a person from `person-map` with events dated according to `generation-n`"
  [{:as person-map
    id :id
    user :associated-username} generation-n]
  ;;(log/info (str "Initialize-person received person-map " (prn-str person-map)))
  (let [event-map {:username user
                   :person-id id
                   :generation-n generation-n
                   :birth-year (:birth (event-dates generation-n))}]
    (people/CREATE person-map)
    (generate-birth event-map)
    (generate-random-event event-map)
    (generate-random-event event-map)))


(defn initialize-user!
  "Given `user-map`, clear any existing user with the same id, then create user, person, and matching events"
  [{:as user-map
    id :id}]
  (let [clean-user-map (select-keys user-map [:email :first-name :gender :id :last-name :password :person-id])
        person-map (derive-person-map clean-user-map)]
    (users/DELETE id)
    (users/CREATE clean-user-map)
    (initialize-person! person-map 0)
    (users/UPDATE id {:person-id (:id person-map)})))


(defn do-marriage
  "Update `father-id` and `mother-id` as spouses to each other, and provide them each with matching marriage events`"
  [{:as marriage-request
    :keys [father-id mother-id username marriage-year]}]
  (doseq [idk [:father-id :mother-id]]
    (when (empty? (people/READ (idk marriage-request)))
      (throw (ex-info (str (name idk) " person does not exist: " (idk marriage-request)) (merge {:all-people (people/READ)} (select-keys marriage-request [idk]))))))

  (let [location-map (random-location)]    
      (doseq [id [father-id mother-id]]        
          (let [event-map (merge location-map 
                                 {:id (str (UUID/randomUUID))
                                  :associated-username username
                                  :person-id id
                                  :event-type "marriage"
                                  :event-year marriage-year})]
            (try
              (events/CREATE event-map)
              (catch Exception e
                (throw (ex-info "Error establishing marriage" {:error (str e)
                                                               :event-map event-map
                                                               :marriage-request marriage-request})))))))
  (people/UPDATE father-id {:spouse-id mother-id})
  (people/UPDATE mother-id {:spouse-id father-id}))

(defn bare-child
  "Establish `father-id` and `mother-id` as the parents of `child-id. Child already has a birth event"
  [{:keys [father-id mother-id child-id]}]
  (people/UPDATE child-id {:father-id father-id
                           :mother-id mother-id}))

 (defn create-parents
  "Create and properly initialize the persons representing the parents of `child-id` associated with `user-id`, and update `child-id` for these parents, according to `generation-n`. Return map of `[:father-id :mother-id]`"
  [user-id child-id generation-n]
  (let [[father-id mother-id] (take 2 (repeatedly random-id))
        fatherm {:id father-id
                :associated-username user-id
                :first-name (gen-male-given-name)
                :last-name (gen-surname)
                :gender "m"}
        motherm {:id mother-id
                :associated-username user-id
                :first-name (gen-female-given-name)
                :last-name (gen-surname)
                 :gender "f"}
        couple-map {:father-id father-id
                    :mother-id mother-id
                    :child-id child-id
                    :username user-id
                    :marriage-year (calculate-parent-marriage-year child-id)}
        return-value [father-id mother-id]]
    (do
      (initialize-person! fatherm generation-n)
      (initialize-person! motherm generation-n)
      (do-marriage couple-map)
      (bare-child couple-map))
    return-value))

(defn genesis
  "Create `num-generations` generations starting with `person-id`"
  [person-id num-generations]
  (if-let [user-id (people/READ person-id [:associated-username])]
    (let [NEED-PARENTS (atom [person-id])]
      (dotimes [i num-generations]
        ;(log/info (str "Here I ought to create generation " i " from " @NEED-PARENTS))
        (let [new-parents (into [] (mapcat #(create-parents user-id % i) @NEED-PARENTS))]
          (reset! NEED-PARENTS new-parents))))
    (throw (ex-info (str "genesis unable to find associated user for " person-id) {:person-id person-id :num-generations num-generations}))))


(defn initialize-and-genesis!
  "Create a new user and associated person based on `user-map`, and `num-generations` of ancestors. `num-generations` defaults to 4."
  ([user-map] (initialize-and-genesis! user-map 4))
  ([{:as user-map
     :keys [password first-name last-name gender id person-id]}
    num-generations]

   (initialize-user! user-map)
   (genesis (users/READ id [:person-id]) num-generations)))
