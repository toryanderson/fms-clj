DROP TABLE IF EXISTS users;
--;;
CREATE TABLE users (
id TEXT PRIMARY KEY NOT NULL UNIQUE CHECK (id <> '') 
, password TEXT CHECK (password <> '')
, email TEXT CHECK (email <> '')
, first_name TEXT CHECK (first_name <> '')
, last_name TEXT CHECK (last_name <> '')
, gender TEXT CHECK(gender = 'f' OR gender = 'm')
, person_id TEXT UNIQUE CHECK (person_id <> '')
, FOREIGN KEY(person_id) REFERENCES people(id)
);
--;;
DROP TABLE IF EXISTS authorization_tokens;
--;;
CREATE TABLE authorization_tokens (
id TEXT PRIMARY KEY UNIQUE NOT NULL
, username TEXT NOT NULL
, FOREIGN KEY(username) REFERENCES users(id) ON DELETE CASCADE
);
--;;
DROP TABLE IF EXISTS people;
--;;
CREATE TABLE people (
id TEXT PRIMARY KEY UNIQUE CHECK (id <> '')
, associated_username TEXT NOT NULL
, first_name TEXT NOT NULL CHECK (first_name <> '')
, last_name TEXT NOT NULL CHECK (last_name <> '')
, gender TEXT NOT NULL CHECK(gender = 'f' OR gender = 'm')
, father_id TEXT
, mother_id TEXT
, spouse_id TEXT
, FOREIGN KEY(associated_username) REFERENCES users(id) ON DELETE CASCADE
, FOREIGN KEY(father_id) REFERENCES people(id) ON DELETE SET NULL
, FOREIGN KEY(mother_id) REFERENCES people(id) ON DELETE SET NULL
, FOREIGN KEY(spouse_id) REFERENCES people(id) ON DELETE SET NULL
);
--;;
DROP TABLE IF EXISTS events;
--;;
CREATE TABLE events (
id TEXT NOT NULL UNIQUE PRIMARY KEY CHECK (id <> '')
, associated_username TEXT NOT NULL
, person_id TEXT NOT NULL
, latitude REAL NOT NULL
, longitude REAL NOT NULL
, country TEXT NOT NULL
, city TEXT NOT NULL
, event_type TEXT NOT NULL
, event_year INT NOT NULL
, FOREIGN KEY(associated_username) REFERENCES users(id) ON DELETE CASCADE
, FOREIGN KEY(person_id) REFERENCES people(id)
);
