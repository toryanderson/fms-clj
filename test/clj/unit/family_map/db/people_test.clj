(ns family-map.db.people-test
  "Tests for the people \"DAO\""
  (:require [family-map.db.people :as subj]
            [family-map.db.users :as users]
            [clojure.test :refer [deftest is testing]]
            [family-map.db.test-util :as tcore]))

(tcore/basic-transaction-fixtures
  (users/CREATE tcore/user-map))



(deftest CREATE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (testing "Create one"
    (is (= 1 (count (subj/CREATE tcore/person-map)))))
  (testing "Fails to read with duplicate id"
    (is (thrown? org.sqlite.SQLiteException  
                 (subj/CREATE tcore/person-map))))
  (testing "Fails to create with invalid gender"
    (is (thrown? org.sqlite.SQLiteException (subj/CREATE (assoc tcore/person-map :gender "INVALID"))) )))

(deftest READ
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE tcore/person-map)
  (is (= (tcore/person-map "PersonYear")
         (:personyear (subj/READ (tcore/person-map "Person ID")))))
  (testing "Read all"
    (subj/CREATE (assoc tcore/person-map :id "Terminator"))
    (is (= 2 (count (subj/READ))))))

(deftest UPDATE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE tcore/person-map)
  (let [id (tcore/person-map :id)
        new-gender "f"]
    (subj/UPDATE id {:gender new-gender})
    (is (= new-gender
           (:gender (subj/READ id))))))

(deftest DELETE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE tcore/person-map)
  (testing "Delete 1"
    (let [id (:id tcore/person-map)]
      (is (= 1 (first (subj/DELETE id))))
      (is (= 0 (count (subj/READ))))
      (testing "Bad delete does nothing"
        (is (= 0 (first (subj/DELETE id)))))))
  (testing "Delete all entries in table"
    (testing ">> Add many entries"
      (let [num 5]
        (doseq [i (map str (range num))]
          (subj/CREATE (assoc tcore/person-map :id i)))
        (is (= 5 (count (subj/READ))))))
    (testing ">> All are deleted"
      (subj/DELETE)
      (is (= 0 (count (subj/READ)))))))

