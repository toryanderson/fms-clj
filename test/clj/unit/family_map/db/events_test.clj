(ns family-map.db.events-test
  "Tests for the events \"DAO\""
  (:require [family-map.db.events :as subj]
            [clojure.test :refer [is testing deftest]]
            [family-map.db.test-util :as tcore]
            [family-map.db.users :as users]
            [family-map.db.people :as people]))

(tcore/basic-transaction-fixtures
 (users/CREATE tcore/user-map)
 (people/CREATE tcore/person-map))
 
(def event-map {:id "rambo"
                :associated-username (:id tcore/user-map)
                :person-id (:id tcore/person-map)
                :latitude 100293.65
                :longitude 100293.65
                :country "Ukraine"
                :city "Kiev"
                :event-type "birth"
                :event-year 2019})


(deftest test-CREATE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (testing "Create one"
    (is (= 1 (count (subj/CREATE event-map)))))
  (testing "Fails to create with invalid id"
    (is (thrown? org.sqlite.SQLiteException (subj/CREATE (assoc event-map :id ""))) )))

(deftest test-READ
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE event-map)
  (is (= (event-map "EventYear")
         (:eventyear (subj/READ (:id event-map)))))
  (testing "Read all"
    (subj/CREATE (assoc event-map :id "Terminator"))
    (is (= 2 (count (subj/READ)))))
  (testing "JDBC has bad read fail quietly"
    (is (nil? (subj/READ 1)))))

(deftest test-UPDATE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE event-map)
  (let [id (event-map :id)
        new-year 1999]
    (subj/UPDATE id {:event-year new-year})
    (is (= new-year
           (:event-year (subj/READ id))))))

(deftest test-DELETE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE event-map)
  (testing "Delete 1"
    (let [id (:id event-map)]
      (is (= 1 (first (subj/DELETE id))))
      (is (= 0 (count (subj/READ))))
      (testing "Bad delete does nothing"
        (is (= 0 (first (subj/DELETE id)))))))
  (testing "Delete all entries in table"
    (testing ">> Add many entries"
      (let [num 5]
        (doseq [i (map str (range num))]
          (subj/CREATE (assoc event-map :id i)))
        (is (= 5 (count (subj/READ))))))
    (testing ">> All are deleted"
      (subj/DELETE)
      (is (= 0 (count (subj/READ)))))))


(deftest test-get-for
  (subj/CREATE event-map)
  (subj/CREATE (assoc event-map :id "second" :event-type "Christening"))
  (testing "Gets two events for user"
    (is (= 2 (count (subj/get-for (:person-id event-map))))))
  (testing "Get the event by type"
    (is (= 1 (count (subj/get-for (:person-id event-map) (:event-type event-map)))))))

(deftest test-birthyear
  (subj/CREATE event-map)
  (testing "Get birthyear"
    (is (int? (subj/birthyear (:person-id event-map))))))
