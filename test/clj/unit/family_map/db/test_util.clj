(ns family-map.db.test-util
  (:require [clojure.test :as t :refer [deftest is testing]]
            [mount.core :as mount]
            [clojure.java.jdbc]
            ;;[family-map.db.migrations]
            [family-map.config :refer [env]]
            [family-map.db.core :refer [*db*] :as db]
            [clojure.math.numeric-tower :as math]
            [clojure.java.io :as io]
            [cheshire.core :as json])
  (:import (clojure.lang IDeref)))

(defmacro with-transaction
  "Runs the body in a transaction where t-conn is the name of the transaction connection.
   The body will be evaluated within a binding where conn is set to the transactional
   connection. The isolation level and readonly status of the transaction may also be specified.
   (with-transaction [conn {:isolation level :read-only? true}]
     ... t-conn ...)
   See jdbc/db-transaction* for more details on the semantics of the :isolation and
   :read-only? options."
  [[dbsym & opts] & body]
  `(if (instance? IDeref ~dbsym)
     (clojure.java.jdbc/with-db-transaction [t-conn# (deref ~dbsym) ~@opts]
       (binding [~dbsym (delay t-conn#)]
         ~@body))
     (clojure.java.jdbc/with-db-transaction [t-conn# ~dbsym ~@opts]
       (binding [~dbsym t-conn#]
         ~@body))))

(defmacro basic-transaction-fixtures
  [& forms]
  `(clojure.test/use-fixtures
     :each
     (fn [f#]
       (mount.core/start
        #'family-map.config/env        
        #'family-map.db.core/*db*)
       (family-map.db.test-util/with-transaction [family-map.db.core/*db*]
         (clojure.java.jdbc/db-set-rollback-only! family-map.db.core/*db*)
         (do ~@forms)
         (f#)))))


(def user-map  {:email "me@example.com"
                :first-name "John"
                :gender "m"
                :id "rambo"
                :last-name "Carter"
                :password "rambo999"
                :person-id "rambo"})

(def person-map {:id  (:id user-map)
                 :associated-username (:id user-map)
                 :first-name "First"
                 :last-name "Person"
                 :gender "m"})

(defn expected-population-at-generation
  "The number of people who should be recorded by `num-generations`"
  [num-generations & include-self?]
  (cond-> 0
    (> num-generations 0)
    (+ (reduce + (map (partial math/expt 2) (range 1 (inc num-generations)))))

    include-self? inc))

(t/deftest expected-population-at-generation-test
  (is (= 0 (expected-population-at-generation 0)))
  (is (= 1 (expected-population-at-generation 0 :self)))
  (is (= 3 (expected-population-at-generation 1 :self)))
  (is (= 6 (expected-population-at-generation 2)))
  (is (= 15 (expected-population-at-generation 3 :self)))
  (is (= 31 (expected-population-at-generation 4 :self))))

(def example-from-json
  "Get the example data provided by json, as string"
  (let [json-file (io/resource "json/example.json")]
    (-> json-file slurp)))

(def clojure-sample-data
  (json/parse-string example-from-json keyword))

