(ns family-map.db.users-test
  "Tests for the users \"DAO\""
  (:require [family-map.db.users :as subj]
            [clojure.test :refer :all]
            [family-map.db.test-util :as tcore]
            [family-map.data :as data]
            [clojure.java.jdbc :as jdbc]
            [mount.core :as mount]
            [family-map.db.core :as db]
            [clojure.tools.logging :as log]
            [honeysql.core :as sql]))

(tcore/basic-transaction-fixtures)

(def user-map {:id "rambo"
               :password "rambo999"
               :email "me@example.com"
               :first-name "John"
               :last-name "Carter"
               :gender "m"
               ;:person-id "dunno"
               })

(deftest test-CREATE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (testing "Create one"
    (is (= 1 (count (subj/CREATE user-map)))))
  (testing "Fails to create with invalid gender"
    (is (thrown? org.sqlite.SQLiteException (subj/CREATE (assoc user-map :gender "INVALID"))) )))

(deftest test-READ
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (data/initialize-user! user-map)
  (is (= (user-map "UserYear")
         (:useryear (subj/READ (user-map "User ID")))))
  (testing "Read all"
    (data/initialize-user! (assoc user-map :id "Terminator"))
    (is (= 2 (count (subj/READ))))))

(deftest test-UPDATE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE user-map)
  (let [id (user-map :id)
        new-gender "f"]
    (subj/UPDATE id {:gender new-gender})
    (is (= new-gender
           (:gender (subj/READ id))))))

(deftest test-DELETE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE user-map)
  (testing "Delete 1"
    (let [id (:id user-map)]
      (is (= 1 (first (subj/DELETE id))))
      (is (= 0 (count (subj/READ))))
      (testing "Bad delete does nothing"
        (is (= 0 (first (subj/DELETE id)))))))
  (testing "Delete all entries in table"
    (testing ">> Add many entries"
      (let [num 5]
        (doseq [i (map str (range num))]
          (subj/CREATE (assoc user-map :id i ; :person-id i
                              )))
        (is (= 5 (count (subj/READ))))))
    (testing ">> All are deleted"
      (subj/DELETE)
      (is (= 0 (count (subj/READ)))))))

(deftest test-valid-password
  (subj/CREATE user-map)
  (testing "Valid pass"
    (is (subj/valid-password (:id user-map) (:password user-map))))
  (testing "Invalid pass"
    (is (not (subj/valid-password (:id user-map) (:password "wrong")))))
  (testing "Invalid user"
    (is (not (subj/valid-password 123 (:password user-map))))
    (is (not (subj/valid-password nil nil)))))
