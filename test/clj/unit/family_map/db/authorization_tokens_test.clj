(ns family-map.db.authorization-tokens-test
  "Tests for the tokens \"DAO\""
  (:require [family-map.db.authorization-tokens :as subj]
            [clojure.test :refer [deftest testing is]]
            [family-map.db.test-util :as tcore]
            [family-map.db.users :as users])
  (:import [java.util
            UUID]))

(tcore/basic-transaction-fixtures
 (users/CREATE tcore/user-map))

(def token-map {:id (str (UUID/randomUUID))
                :username (:id tcore/user-map)})

(deftest test-CREATE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (testing "Create one"
    (is (= 1 (count (subj/CREATE token-map)))))
  (testing "Fails to create with invalid id"
    ;(is (thrown? org.sqlite.SQLiteException (subj/CREATE (dissoc token-map :id))) )
    ))

(deftest test-READ
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE token-map)
  (testing "Now one"
    (is (= 1 (count (subj/READ)))))
  (testing "Read all"
    (subj/CREATE (assoc token-map :id "Terminator"))
    (is (= 2 (count (subj/READ)))))
  (testing "JDBC has bad read fail quietly"
    (is (nil? (subj/READ 1)))))

(deftest test-UPDATE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE token-map)
  (testing "Update the id works"
    (let [id (token-map :id)
          new-token (str (UUID/randomUUID))]
      (subj/UPDATE id {:id new-token})
      (is (nil? (:id (subj/READ id))))
      (is (= new-token (:id (subj/READ new-token)))))))

(deftest test-DELETE
  (testing "Starts empty"
    (is (= 0 (count (subj/READ)))))
  (subj/CREATE token-map)
  (testing "Delete 1"
    (let [id (:id token-map)]
      (is (= 1 (first (subj/DELETE id))))
      (is (= 0 (count (subj/READ))))
      (testing "Bad delete does nothing"
        (is (= 0 (first (subj/DELETE id)))))))
  (testing "Delete all entries in table"
    (testing ">> Add many entries"
      (let [num 5]
        (doseq [i (map str (range num))]
          (subj/CREATE (assoc token-map :id i)))
        (is (= 5 (count (subj/READ))))))
    (testing ">> All are deleted"
      (subj/DELETE)
      (is (= 0 (count (subj/READ)))))))

(deftest test-generate-token
  (let [new-token (subj/generate-token (:id tcore/user-map))]
    (testing "Generated new token"
      (is (some? new-token))
      (is (= 1 (count (subj/READ)))))
    (testing "New token is initialized with a valid id"
      (testing ">> Is a string"
        (is (string? (:id new-token))))
      (testing ">> Is not empty"
        (is (not-empty (:id new-token)))))))

(deftest test-tokens-for-user
  (let [person-id (:id tcore/user-map)
        _new-token (subj/generate-token person-id)]
    (testing "One token retrieved for user"
      (is (= 1 (count (subj/tokens-for-user person-id)))))
    (testing "multiple tokens retrieved for user (including one created above)"
      (let [n 3]
        (doall (repeatedly n #(subj/generate-token person-id)))
        (is (= (inc n) (count (subj/tokens-for-user person-id))))))
    (testing "Fails without user-id provided"
      (is (empty? (subj/tokens-for-user nil))))))
