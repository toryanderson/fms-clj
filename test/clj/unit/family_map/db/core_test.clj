(ns family-map.db.core-test
  "Tests for the core db"
  (:require [family-map.db.core :as subj]
            [clojure.test :refer [is testing deftest]]
            [family-map.db.test-util :as tcore]
            [family-map.db.users :as users]))

(tcore/basic-transaction-fixtures)

(deftest unique-constraint
  (users/CREATE tcore/user-map)
  (is (thrown? org.sqlite.SQLiteException (users/CREATE tcore/user-map))))

 

