(ns family-map.routes.api-test
  (:require [family-map.routes.api :as subj]
            [family-map.db.authorization-tokens :as tokens]
            [family-map.db.events :as events]
            [family-map.db.users :as users]
            [family-map.db.people :as people]
            [family-map.db.test-util :as tcore]
            [family-map.data :as data]
            [clojure.test :refer [is deftest testing]]
            [taoensso.timbre :as log]))

(tcore/basic-transaction-fixtures)

(deftest test-get-all-family
  (testing "Init 0"
    (is (= 0 (count (users/READ)))))
  (data/initialize-and-genesis! tcore/user-map)
  (let [population (count (people/READ))
        response (subj/get-all-family {:user-id (:id tcore/user-map)})]
    (testing "we have a population"
      (is (pos? population)))
    (testing "Population matches"
      (is (= population (count (:body response)))))
    (testing "Valid response"
      (is (= 200 (:status response))))))

(deftest test-get-person
  (testing "No users at start"
    (is (= 0 (count (users/READ)))))  
  (users/CREATE tcore/user-map)
  (people/CREATE tcore/person-map)
  (let [person-id (:id tcore/person-map)
        request-essentials {:user-id (:id tcore/user-map)
                            :path-params {:person-id person-id}}
        response (subj/get-person request-essentials)
        response-body (:body response)]
    (testing "Valid"
      (is (= 200 (:status response))))
    (testing "Got my person back"
      (is (not-empty response-body))
      (is (= (response-body "personID")
             (:id tcore/person-map))))))

(deftest test-get-event
  (testing "No events at start"
    (is (= 0 (count (events/READ)))))  
  (data/initialize-and-genesis! tcore/person-map)
  (let [event-id (-> (events/READ) first :id)
        request-essentials {:user-id (:id tcore/user-map)
                            :path-params {:event-id event-id}}
        response (subj/get-event request-essentials)
        response-body (:body response)]
    (testing "Valid"
      (is (= 200 (:status response))))
    (testing "Got my event back"
      (is (not-empty response-body))
      (is (= (response-body "eventID")
             event-id)))))

(deftest test-get-all-events
  (testing "Init 0"
    (is (= 0 (count (users/READ)))))
  (data/initialize-and-genesis! tcore/user-map)
  (let [event-count (count (events/READ))
        response (subj/get-all-events {:user-id (:id tcore/user-map)})]
    (testing "we have a population"
      (is (pos? event-count)))
    (testing "Population matches"
      (is (= event-count (count (:body response)))))
    (testing "Valid response"
      (is (= 200 (:status response))))))






