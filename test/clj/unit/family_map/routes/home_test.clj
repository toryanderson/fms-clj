(ns family-map.routes.home-test
  (:require [family-map.routes.home :as subj]
            [family-map.db.authorization-tokens :as tokens]
            [family-map.db.events :as events]
            [family-map.db.users :as users]
            [family-map.db.people :as people]
            [family-map.db.test-util :as tcore]
            [family-map.data :as data]
            [clojure.test :refer [is deftest testing]]
            [taoensso.timbre :as log]))


(tcore/basic-transaction-fixtures)

(def register-input
  "Input compatible for register routes"
  {:userName "susan",
   :password "mysecret",
   :email "susan@gmail.com",
   :firstName "Susan",
   :lastName "Ellis",
   :gender "f"})

(deftest test-home-page
  (testing "Homepage successfully returns an html body"
    (let [result (subj/home-page nil)]
      (is (= 200 (:status result)))
      (is ((every-pred not-empty string?) (:body result))))))

(deftest test-register-user
  (testing "Init 0"
    (is (= 0 (count (users/READ)))))
  (let [failed-input (dissoc register-input :password)
        request {:params register-input}
        response (subj/register-user request)
        failed-response (subj/register-user {:params failed-input})]
    (testing "New user created"
      (is (= 1 (count (users/READ)))))

    (testing "4 generations created"
      (is (= (tcore/expected-population-at-generation 4 :self)
             (count (people/READ)))))

    (testing "Receive appropriate request to register"
      #_(log/info (str "Register response is: " (prn-str response)))
      (is (= 200 (:status response)))
      (is ((every-pred not-empty string?) (get-in response [:body "authToken"])))
      #_(log/info (str "\n>> Response is:\n "
                     (prn-str response)))      )
    (testing "Return appropriate error response"
      (is (= 400 (:status failed-response)))
      (is (string? (get-in failed-response [:body "description"]))))))

(deftest test-user-login
  (let [_create-user (users/CREATE tcore/user-map)
        valid-login (subj/user-login {:params {:userName (:id tcore/user-map)
                                               :password (:password tcore/user-map)}})
        invalid-login (subj/user-login {:params {:userName (:id tcore/user-map)
                                                 :password "WRONG"}})
        bad-params (subj/user-login {:userName (:id tcore/user-map)
                                     :password "WRONG"})]
    (testing "Correct login"
      (is (= 200 (:status valid-login)))
      (is (string? (-> valid-login :body (get "authToken")))))
    
    (testing "Incorrect login"
      (is (= 401 (:status invalid-login)))
      (is ((complement string?) (-> invalid-login :body (get "authToken")))))

    (testing "Bad params"
      (is (= 401 (:status bad-params)))
      (is ((complement string?) (-> bad-params :body (get "authToken")))))))

(deftest test-fill-generations
  (testing "init: no users"
    (is (= 0 (count (users/READ)))))
  (let [generations-n 4
        _init-user (data/initialize-and-genesis! tcore/user-map) 
        fill-generations (subj/fill-generations
                          {:path-params {:username (:id tcore/user-map)
                                         :generations generations-n}})
        expected-message (format "Successfully added %d persons and %d events to the database."
                                 (tcore/expected-population-at-generation generations-n)
                                 (+ 3 (* 4 (tcore/expected-population-at-generation generations-n))))]
    (testing "Population increased"
      (is (= (tcore/expected-population-at-generation generations-n :self)
             (count (people/READ)))))
    (testing "Correct change reported"
      (is (= expected-message (-> fill-generations :body (get "message")))))))

(deftest test-clear-db
  (testing "Initial empty"
    (is (= 0 (count (users/READ)))))
  (let [_register (subj/register-user {:params register-input})
        pre-vals {:events (count (events/READ))
                  :tokens (count (tokens/READ))
                  :people (count (people/READ))
                  :users (count (users/READ))}
        _ (do (log/info "pre Users:\n" (prn-str (users/READ)))
              (log/info "pre People:\n" (prn-str (people/READ))))
        response (subj/clear-db nil)
        _ (do (log/info "\n\npost Users:\n" (prn-str (users/READ)))
              (log/info "post People:\n" (prn-str (people/READ))))
        post-vals {:events (count (events/READ))
                   :tokens (count (tokens/READ))
                   :people (count (people/READ))
                   :users (count (users/READ))}]
    (log/info (str "register response was:\n " (prn-str _register)))
    (testing "DB was populated"
      (log/info "Prevals are:\n" (prn-str pre-vals))
      (is (every? pos? (vals pre-vals))))
    (testing "DB cleared"
      (log/info "Postvals are:\n" (prn-str post-vals))
      (is (every? zero? (vals post-vals))))
    (testing "Response correct"
      (is (= 200 (:status response)))
      (is (= "Clear succeeded." (-> response :body (get "message")))))))

(deftest test-load-db  
  (let [pre-population {:users (count (users/READ))
                        :events (count (events/READ))
                        :people (count (people/READ))}
        _init-population (data/initialize-and-genesis! tcore/user-map)
        json-data tcore/clojure-sample-data
        input {:params json-data}
        _load (subj/load-db input)
        population {:users (count (users/READ))
                    :events (count (events/READ))
                    :people (count (people/READ))}]
    (testing "Nothing at init"
      (is (every? zero? (vals pre-population))))
    (testing "data got loaded"
      (is (every? pos? (vals population))))))
