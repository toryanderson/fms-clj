(ns family-map.data-test
  (:require [family-map.data :as subj]
            [family-map.db.test-util :refer [expected-population-at-generation] :as tcore]
            [family-map.db.people :as people]
            [family-map.db.events :as events]
            [family-map.db.authorization-tokens :as tokens]
            [family-map.db.users :as users]
            [clojure.test :refer [is deftest testing]]))

(tcore/basic-transaction-fixtures)

(deftest test-SURNAMES
  (testing "Correctly initialized surnames"
    (is (< 1 (count subj/SURNAMES)))))

(deftest test-MALE-GIVEN-NAMES
  (testing "Correctly initialized male given names"
    (is (< 1 (count subj/MALE-GIVEN-NAMES)))))

(deftest test-FEMALE-GIVEN-NAMES
  (testing "Correctly initialized female given names"
    (is (< 1 (count subj/FEMALE-GIVEN-NAMES)))))

(deftest test-clear-for-user
  (testing "No users yet"
    (is (= 0 (count (users/READ)))))  
  (testing "db populated"
    (subj/initialize-and-genesis! tcore/user-map)
    (tokens/generate-token (:id tcore/user-map))
    (let [pre-vals {:events (count (events/READ))
                    :tokens (count (tokens/READ))
                    :people (count (people/READ))
                    :users (count (users/READ))}
          _empty (subj/clear-for-user (:id tcore/user-map))
          post-vals {:events (count (events/READ))
                     :tokens (count (tokens/READ))
                     :people (count (people/READ))
                     :users (count (users/READ))}]
      (testing "DB was populated"
        (is (every? pos? (vals pre-vals))))
      (testing "DB empty after clear"
        (is (every? zero? (vals post-vals))))))
  (testing "clears only one user's stuff"
    (let [_user1 (do (subj/initialize-and-genesis! tcore/user-map)
                     (tokens/generate-token (:id tcore/user-map)))
          m2 (assoc tcore/user-map :id "Other user" :person-id "some other")
          _user2  (do (subj/initialize-and-genesis! m2)
                      (tokens/generate-token (:id m2)))
          pre-vals {:events (count (events/READ))
                    :tokens (count (tokens/READ))
                    :people (count (people/READ))
                    :users (count (users/READ))}
          _empty (subj/clear-for-user (:id tcore/user-map))
          post-vals {:events (count (events/READ))
                     :tokens (count (tokens/READ))
                     :people (count (people/READ))
                     :users (count (users/READ))}]
      (testing "DB was populated"
        (is (every? pos? (vals pre-vals))))
      (testing "DB not empty after clear"
        (is (not= pre-vals post-vals))
        (is (every? pos? (vals post-vals)))))))


(deftest test-initialize-user!
  (testing "No users yet"
    (is (= 0 (count (users/READ)))))
  (subj/initialize-user! tcore/user-map)
  (testing "Create user"
    (is (= 1 (count (users/READ)))))
  (testing "Person initialized"
    (is (= 1 (count (people/READ)))))
  (testing "Events initialized"
    (is (= 3 (count (events/READ)))))
  (testing "User gets default person even without person-id"
    (subj/clear-db!)
    (subj/initialize-user! (dissoc tcore/user-map :person-id))
    (is (= 1 (count (people/READ))))
    (is (= (:id tcore/user-map) (-> tcore/user-map :id (users/READ [:person-id]))))))

(deftest test-do-marriage
  (let [father-map (merge tcore/person-map {:id "father1"
                                            :gender "m"}) 
        mother-map (merge tcore/person-map {:id "mother1"
                                            :gender "f"})]
    (subj/initialize-and-genesis! tcore/user-map 0)
    (people/CREATE father-map)
    (people/CREATE mother-map)
    (testing "Change spouse status of person"
      (subj/do-marriage {:father-id (:id father-map)
                         :mother-id (:id mother-map)
                         :marriage-year 1999
                         :child-id (:id tcore/user-map)
                         :username (:id tcore/user-map)} )
      (is (= (:id mother-map)
             (-> father-map :id people/READ :spouse-id))))))


(deftest test-genesis
  (let [num-generations 5
        _U! (subj/initialize-user! tcore/user-map)
        person-id (users/READ (:id tcore/user-map) [:person-id])
        _C! (subj/genesis person-id num-generations)
        all-people (people/READ)]
    (testing "Created generations"
      (is (= (expected-population-at-generation num-generations :self)
             (count all-people))))
    (testing "Sufficient events created for every generation"
      (is (< (* 3 (expected-population-at-generation num-generations :self))
             (count (events/READ)))))))


(deftest test-initialize-and-genesis!
  (let [num-generations 4]
    (testing "Initialize everything, user, people, and events"
      (subj/initialize-and-genesis! tcore/user-map num-generations)
      (testing "Number of users"
        (is (= 1 (count (users/READ)))))
      (testing "Number of people"
        (is (= (expected-population-at-generation num-generations :self)
               (count (people/READ)))))
      (testing "Number of events"
        (is (< (* 3 (expected-population-at-generation num-generations :self))
               (count (events/READ))))))))

(deftest test-initialize-and-genesis-without-generations
  (let [num-generations 4]
    (testing "Initialize everything, user, people, and events"
      (subj/initialize-and-genesis! tcore/user-map)
      (testing "Number of users"
        (is (= 1 (count (users/READ)))))
      (testing "Number of people"
        (is (= (expected-population-at-generation num-generations :self)
               (count (people/READ)))))
      (testing "Number of events"
        (is (< (* 3 (expected-population-at-generation num-generations :self))
               (count (events/READ))))))))

(deftest test-load-data
  (testing "No users yet"
    (is (= 0 (count (users/READ)))))  
  (let [data {:users [{:userName "sheila", :password "parker", :email "sheila@parker.com", :firstName "Sheila", :lastName "Parker", :gender "f", :personID "Sheila_Parker"}], :persons [{:firstName "Sheila", :lastName "Parker", :gender "f", :personID "Sheila_Parker", :father "Patrick_Spencer", :mother "Im_really_good_at_names", :descendant "sheila"} {:firstName "Patrick", :lastName "Spencer", :gender "m", :personID "Patrick_Spencer", :spouse "Im_really_good_at_names", :descendant "sheila"} {:firstName "CS240", :lastName "JavaRocks", :gender "f", :personID "Im_really_good_at_names", :spouse "Patrick_Spencer", :descendant "sheila"}], :events [{:eventType "started family map", :personID "Sheila_Parker", :descendant "sheila", :city "Salt Lake City", :longitude -110.1167, :year 2016, :latitude 40.75, :country "United States", :eventID "Sheila_Family_Map"} {:eventType "fixed this thing", :personID "Patrick_Spencer", :descendant "sheila", :city "Provo", :longitude -111.6585, :year 2017, :latitude 40.2338, :country "United States", :eventID "I_hate_formatting"}]}]
    (subj/load-data-from-json data)
    (testing "Now a user"
      (is (= (count (:users data))
             (count (users/READ)))))
    (testing "Now people"
      (is (= (count (:persons data))
             (count (people/READ)))))
    (testing "Now events"
      (is (= (count (:events data))
             (count (events/READ)))))))
