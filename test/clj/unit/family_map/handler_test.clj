(ns family-map.handler-test
  (:require [family-map.handler :as subj]
            [ring.mock.request :as mock]
            [family-map.db.test-util :refer [example-from-json] :as tcore]
            [family-map.db.users :as users]
            [family-map.db.people :as people]
            [family-map.db.events :as events]
            [family-map.data :as data]
            [clojure.test :refer [is deftest testing]]
            [cheshire.core :as json]
            [taoensso.timbre :as log]
            [clojure.java.io :as io]
            [mount.core]))

(tcore/basic-transaction-fixtures
 (mount.core/start #'family-map.handler/app))

(defn make-json-request [route data]
  (let [data-in (if (string? data) data (json/generate-string data))]
    (-> (mock/request :post route data-in)
        (mock/content-type "application/json"))))

(defn read-json-response [response]
  (-> response :body clojure.java.io/reader json/parse-stream))

(deftest home-route
  (testing "Serves HTML "
    (let [request (mock/request :get "/")
          response (subj/app request)]
      (is (= 200 (:status response)))
      (is (re-find #"text/html"
                   (get-in response [:headers "Content-Type"]))))))

(deftest register-route
  (let [route "/user/register"
        input {"userName" "susan",
               "password" "mysecret",
               "email" "susan@gmail.com",
               "firstName" "Susan",
               "lastName" "Ellis",
               "gender" "f"}
        failed-input (dissoc input "password")        
        request (make-json-request route input)
        response (subj/app request)]
    (log/info (str "Request is:" (prn-str request)))
    (testing "Receive appropriate response to register"
      (testing ">> Request processed correctly"
        (is (= 200 (:status response))))
      (testing ">> Response has authcode"
        #_(log/info (str "\n>> request is:\n "
                     (prn-str request)))
        #_(log/info (str "\n>> Response is:\n "
                     (prn-str response)))
        (is ((every-pred not-empty string?)
             (-> response :body clojure.java.io/reader (json/parse-stream) (get "authToken"))))))
    (testing "Return appropriate error json"
      (is (= 400 (:status (subj/app (make-json-request route failed-input))))))))

(deftest user-login-route
  (users/CREATE (dissoc  tcore/user-map :person-id))
  (let [input {"userName" (:id tcore/user-map)
               "password" (:password tcore/user-map)}
        route "/user/login"
        request (make-json-request route input)
        bad-request (->> (assoc input "password" "")
                         (make-json-request route))
        response (subj/app request)
        bad-response (subj/app bad-request)]
    (testing "Valid response with auth-code"
      (is (= 200 (:status response)))
      (is ((every-pred not-empty string?)
           (-> response :body clojure.java.io/reader (json/parse-stream) (get "authToken")))))
    (testing "Missing/incorrect password"
      (is (= 401 (:status bad-response)))
      (is (re-find #"Incorrect" (get (read-json-response bad-response) "message"))))))

(deftest clear-route
  (testing "Clear database route"
    (let [route "/clear"
          response (subj/app (mock/request :post route))]
      (is (= 200 (:status response)))
      (is (= "Clear succeeded." (get (read-json-response response) "message"))))))

(deftest load-route
  (testing "Load database route"
    (let [route "/load"
          input (json/parse-string example-from-json) ;example-from-json
          request (make-json-request route input)
          response (subj/app request)
          {some-users "users"
           some-people "persons"
           some-events "events"} (json/parse-string example-from-json)
          gen-response-message #(format "Successfully added %d users, %d persons, and %d events to the database."
                                        (count some-users)
                                        (count some-people)
                                        (count some-events))]
      (is (= 200 (:status response)))
      ;(log/info (str "Response is:\n" (read-json-response response)))
      (is (= (gen-response-message) (get (read-json-response response) "message"))))))

(deftest fill-route
  (testing "No users at start"
    (is (= 0 (count (users/READ)))))
  (data/initialize-and-genesis! tcore/user-map)
  (testing "Now users and people at start"
    (is (= 1 (count (users/READ))))
    (is (= 31 (count (people/READ)))))

  (let [username (:id tcore/user-map)
        generations 4
        gen-route (fn [username generations] (format "/fill/%s/%d" username generations))
        response (subj/app (mock/request :post (gen-route username generations)))        
        bad-response (subj/app (mock/request :post (gen-route username -1)))
        success-message "Successfully added 30 persons and 123 events to the database."]
    (testing "Route succeeds"
      (is (= 200 (:status response)))
      (is (= success-message (get (read-json-response response) "message"))))
    (testing "Route fails"
      (is (= 400 (:status bad-response)))
      (is (re-find #"Invalid" (get (read-json-response bad-response) "message"))))))

(defn login-for-token
  "Obtain the authtoken from a valid login"
  [username password]
  (let [input {"userName" username
               "password" password}
        route "/user/login"
        request (make-json-request route input)
        response (subj/app request)]
    (-> response :body clojure.java.io/reader json/parse-stream (get "authToken"))))

(deftest test-person-route
  (testing "No users at start"
    (is (= 0 (count (users/READ)))))  
  (users/CREATE tcore/user-map)
  (people/CREATE tcore/person-map)
  (let [person-id (:id tcore/person-map)
        route (str "/person/" person-id)
        unauthorized-response (subj/app (mock/request :get route))
        
        login-token (login-for-token (:id tcore/user-map) (:password tcore/user-map))
        valid-response (-> (mock/request :get route)
                           (mock/header "Authorization" login-token)
                           subj/app)]
    (testing "Have token"
      (is ((every-pred not-empty string?) login-token)))
    (testing "Unauthorized"
      (is (= 401 (:status unauthorized-response))))
    (testing "Authorization"
      (is (= 200 (:status valid-response))))
    (testing "got person back"
      (is (map? (read-json-response valid-response))))))

(deftest test-event-route
  (testing "No users at start"
    (is (= 0 (count (users/READ)))))  
  (data/initialize-and-genesis! tcore/user-map)
  (let [event-id (-> (events/READ) first :id)
        route (str "/event/" event-id)
        unauthorized-response (subj/app (mock/request :get route))
        
        login-token (login-for-token (:id tcore/user-map) (:password tcore/user-map))
        valid-response (-> (mock/request :get route)
                           (mock/header "Authorization" login-token)
                           subj/app)]
    (testing "Have event id"
      (is ((every-pred not-empty string?) event-id)))
    (testing "Have token"
      (is ((every-pred not-empty string?) login-token)))
    (testing "Unauthorized"
      (is (= 401 (:status unauthorized-response))))
    (testing "Authorization"
      (is (= 200 (:status valid-response))))
    (testing "got event back"
      (is (map? (read-json-response valid-response))))))

(deftest test-all-person-route
  (testing "No users at start"
    (is (= 0 (count (users/READ)))))
  (data/initialize-and-genesis! tcore/user-map)
  (let [route "/person"
        unauthorized-response (subj/app (mock/request :get route))
        login-token (login-for-token (:id tcore/user-map) (:password tcore/user-map))
        valid-response (-> (mock/request :get route)
                           (mock/header "Authorization" login-token)
                           subj/app)]
    (testing "valid response"
      (is (= 200 (:status valid-response))))
    (testing "Invalid response"
      (is (= 401 (:status unauthorized-response))))))

(deftest test-all-event-route
  (testing "No events at start"
    (is (= 0 (count (events/READ)))))
  (data/initialize-and-genesis! tcore/user-map)
  (let [route "/event"
        unauthorized-response (subj/app (mock/request :get route))
        event-count (count (events/READ))
        login-token (login-for-token (:id tcore/user-map) (:password tcore/user-map))
        valid-response (-> (mock/request :get route)
                           (mock/header "Authorization" login-token)
                           subj/app)]
    (testing "valid response"
      (is (= 200 (:status valid-response))))
    (testing "Invalid response"
      (is (= 401 (:status unauthorized-response))))
    (testing "Got the results"
      (is (pos? event-count))
      (is (= event-count (count (read-json-response valid-response)))))))
