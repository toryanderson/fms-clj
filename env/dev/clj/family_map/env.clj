(ns family-map.env
  (:require 
            [clojure.tools.logging :as log]
            [family-map.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[family-map started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[family-map has shut down successfully]=-"))
   :middleware wrap-dev})
