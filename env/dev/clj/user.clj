(ns user
  (:require [mount.core :as mount]
            family-map.core))

(defn start []
  (mount/start))

(defn stop []
  (mount/stop))

(defn restart []
  (stop)
  (start))


