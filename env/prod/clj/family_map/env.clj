(ns family-map.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[family-map started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[family-map has shut down successfully]=-"))
   :middleware identity})
